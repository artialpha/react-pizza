import React, {useState} from "react";
import Navbar from "./components/Navbar"
import { BrowserRouter as Router } from 'react-router-dom'
import { GlobalStyle } from "./globalStyles";
import Hero from "./components/Hero";
import Products from "./components/Products";
import { productData, productDataTwo } from "./components/Products/data";
import Feature from "./components/Feature";
import Footer from "./components/Footer";

function App() {
  const [cartOfAddedProducts, setCartOfAddedProducts] = useState([])

  return (
    <Router>
      <GlobalStyle/>
      <Hero cartProducts={cartOfAddedProducts} setCartProducts={setCartOfAddedProducts}/>
      <Products heading='Choose your favorite' data={productData} 
      cartProducts={cartOfAddedProducts} setCartProducts={setCartOfAddedProducts}/>
      <Feature/>
      <Products heading='Sweet Treats for you' data={productDataTwo}/>
      <Footer />
    </Router>
  );
}

export default App;
