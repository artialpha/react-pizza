import { createGlobalStyle } from 'styled-components'

export const GlobalStyle = createGlobalStyle`
    * {
        box-sizing: border-box; // https://developer.mozilla.org/pl/docs/Web/CSS/box-sizing
        margin: 0;
        padding: 0;
        font-family: 'Kanit', sans-serif;
    }
`