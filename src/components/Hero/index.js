import React, { useEffect, useState } from 'react';
import Navbar from '../Navbar';
import Sidebar from '../Sidebar';
import { HeroContainer, HeroContent, HeroItems, HeroH1, 
  HeroP, HeroBtn, HeroOrderWrapper, HeroOrderWindow, ProductBadge } from './Heroelements'
  import {
    ProductTitle,
    ProductCard,
    ProductImg,
    ProductInfo,
    ProductPrice,
  } from '../Products/ProductsElements';
  import { Badge } from '@mui/material';


const HeroMain = () => {
  return (
    <HeroItems>
      <HeroH1>Greatest Pizza Ever</HeroH1>
      <HeroP>Ready in 60 seconds</HeroP>
      <HeroBtn>Place Order</HeroBtn>
    </HeroItems>
  )
}


const HeroOrder = ({cartProducts}) => {
  return (
    <HeroOrderWrapper>
      <p>Here's your order</p>
      <HeroOrderWindow>
      {cartProducts.map((product, index) => {
          return (
            <ProductCard key={index}>
              <ProductBadge>
                <Badge badgeContent={'X'} showZero color="primary" anchorOrigin={{
                  vertical: 'top',
                  horizontal: 'left',
                  }}>
                  <ProductImg src={product.img} alt={product.alt} />
                </Badge>
              </ProductBadge>
              <ProductInfo>
                <ProductTitle>{product.name}</ProductTitle>
                <ProductPrice>{product.price}</ProductPrice>
              </ProductInfo>
            </ProductCard>
          );
        })}
      </HeroOrderWindow>
    </HeroOrderWrapper>
    
  )
}



const Hero = ({cartProducts, setCartProducts}) => {
  const [isOpen, setIsOpen] = useState(false)
  const [showHero, setShowHero] = useState(false)

  const toggle = () => {
      setIsOpen(!isOpen)
      console.log("is open")
  }

  const toggleHero = () => {
      setShowHero(!showHero)
      console.log("show hero")
      console.log(cartProducts)
  }
  

  return (
      <HeroContainer >
          <Navbar toggle={toggle} toggleHero={toggleHero} cartProducts={cartProducts}/>
          <Sidebar isOpen={isOpen} toggle={toggle}/>
          <HeroContent>
            { showHero ? <HeroMain/> : <HeroOrder cartProducts={cartProducts}/> }
          </HeroContent>
      </HeroContainer>
  )
};

export default Hero;
