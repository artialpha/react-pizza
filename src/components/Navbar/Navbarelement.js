import styled from 'styled-components'
import { NavLink as Link } from 'react-router-dom'


export const Nav = styled.nav`
    background: purple;
    /*border-style: solid;
    border-color: green;*/
    height: 80px;
    display: flex;
    justify-content: space-between;
    font-weight: 700;
    
`

export const NavLink = styled(Link)`
    background: transparent;
    color: #fff;
    font-size: 2rem;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    text-decoration: none;
    cursor: pointer;
    width: 55%;

    @media screen and (max-width: 400px) {
        position: absolute;
        top: 10px;
        left: 25px;
    }


`

export const NavIcon = styled.div`
    background-color: red;
    padding:5px;
    display: flex;
    align-items: center;
    justify-content: flex-end;
    cursos: pointer;
    color: #fff;
    width: 40%;

    p {
        background-color: green;
        font-weight: bold;
        margin:5px;
    }

`
export const Wrapper = styled.div`
    background: blue;
    font-size: 2rem;
    align-items: center;
    display: flex;
    justify-content: center;
    margin:5px;
`;

