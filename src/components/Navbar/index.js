import React from 'react';
import {Nav, NavLink, NavIcon, Wrapper} from './Navbarelement';
import { FaShoppingCart } from 'react-icons/fa'
import { Badge } from '@mui/material';


const CustomIcon = ({ Icon, xTranslate }) => {
  return (
      <Wrapper xTr={xTranslate}>
        <Icon></Icon>
      </Wrapper>
  );
}


const Navbar = ({ toggle, toggleHero, cartProducts }) => {
  return (
  <>
    <Nav>
        <NavLink to='/'>
          <p>Pizza</p>
        </NavLink>    
        <NavIcon>
          <Badge onClick={toggleHero} badgeContent={cartProducts.length} showZero color="primary"   anchorOrigin={{
            vertical: 'top',
            horizontal: 'left',
          }}>
            <CustomIcon Icon={FaShoppingCart} ></CustomIcon>
          </Badge>
          <p onClick={toggle}>Menu</p>
        </NavIcon>
    </Nav>   
  </>
  );
};

export default Navbar;